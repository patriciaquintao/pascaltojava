import java.util.Scanner;
import java.lang.Math;

public class Simples1 {

	public static void main(String[] args) {
		double raio=0, area=0;
		Scanner teclado = new Scanner (System.in);
		
		System.out.print("Informe o raio do circulo: ");
		raio = teclado.nextDouble();
		
		area = 3.14 * Math.pow(raio,2);
		
		System.out.println("A �rea do c�rculo �: "+area);

	}

}
